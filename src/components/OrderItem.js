import React, { Component } from 'react';
import products from '../api/products';
/**
 * This component is used to display each item in an order.
 */
class OrderItem extends Component {
  constructor(props) {
    super(props);
    this.state = props.item;
    this.state.description = "";
    // for click events from DOM we need to bind `this` variable to keep the same context.
    this.handleQuantityChange = this.handleQuantityChange.bind(this);
    this.handleRemoveItem = this.handleRemoveItem.bind(this);
  }
  /**
   * Load products details
   */
  componentDidMount() {
    this.getProductsDescription();
  }
  /**
   * Update component state when new props are received
   * @param {*} newProps 
   */
  componentWillReceiveProps(newProps) {
    this.setState(newProps.item);
    this.getProductsDescription();
  }
  /**
   * Get product description for current product
   */
  getProductsDescription(){
    let _this = this;
    products.forEach(function(item){
      if( item.id === _this.state['product-id'] ) {
        _this.setState({description: item.description});
      }
    });
  }
  /**
   * Update quantity on this component
   * Also call parent's `onQuantityChange` callback function
   * @param {*} event 
   */
  handleQuantityChange(event){
    let quantity = event.target.value;
    let totalPrice = quantity * this.state['unit-price'];
    totalPrice = parseFloat(Math.round(totalPrice * 100) / 100).toFixed(2);
    this.setState({quantity: quantity});
    this.setState({total: totalPrice});
    let newItem = {
      'product-id': this.state['product-id'],
      'unit-price': this.state['unit-price'],
      'quantity': quantity,
      'total': totalPrice,
    };
    this.props.onQuantityChange(newItem);
  }
  /**
   * Remove product from current order
   * @param {*} event 
   */
  handleRemoveItem(event) {
    let newItem = {
      'product-id': this.state['product-id'],
      'unit-price': this.state['unit-price'],
      'quantity': 0,
      'total': 0,
    };
    this.props.onQuantityChange(newItem);
  }

  render() {
    return (
      <tr key={ this.state['product-id'] }>
        <td><span>{this.state['product-id']}</span></td>
        <td><span>{this.state.description}</span></td>
        <td><span>$ {this.state['unit-price']}</span></td>
        <td><input type='number' step='1' name='quantity' value={this.state.quantity} size='3' onChange={this.handleQuantityChange} /></td>
        <td><span>$ {this.state.total}</span></td>
        <td><button onClick={this.handleRemoveItem}>Remove item</button></td>
      </tr>
    );
  }
}

export default OrderItem;
