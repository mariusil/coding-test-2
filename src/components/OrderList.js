import React, { Component } from 'react';
import OrderForm from './OrderForm';
import orders from '../api/orders';
import customers from '../api/customers';

class OrderList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOrder: {},
      ordersList: [],
      customersList: [] 
    };
    // for click events from DOM we need to bind `this` variable to keep the same context.
    this.handleCancelForm = this.handleCancelForm.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }
  /**
   * Load orders and customers
   */
  componentDidMount() {
    this.setState({ordersList: orders});
    let customersProcessed = [];
    customers.forEach(function(customer){
      customersProcessed[customer.id] = customer;
    });
    this.setState({customersList: customersProcessed});
  }

  /**
  * Set the state for the clicked order
  * @param {*} order 
  */
  handleClick(order) {
    this.setState({
      selectedOrder: order
    })
  }
  /**
   * Determine if the component should re-render
   * component will be re-renderd when user clicks on a different order or when the number of orders is updated
   * @param {*} nextProps 
   * @param {*} nextState 
   */
  shouldComponentUpdate(nextProps, nextState) {
    return nextState.selectedOrder.id !== this.state.selectedOrder.id || nextState.ordersList.length !== this.state.ordersList.length;
  }
  /**
   * Hide order edit form when cancel button is clicked
   */
  handleCancelForm(){
    this.setState({selectedOrder: {}});
  }
  /**
   * Update orders list with the current edited values
   * @param {*} order 
   */
  handleUpdate(order){
    this.setState({selectedOrder: {}});
    let newOrders = [];
    for (var i = 0; i < this.state.ordersList.length; i++ ) {
      if( this.state.ordersList[i].id === order.id ) {
        newOrders.push(order);
      } else {
        newOrders.push(this.state.ordersList[i]);
      }
    }
    this.setState({ordersList: newOrders});
  }

  render() {
    let _this = this;
    let orderForm = this.state.selectedOrder.hasOwnProperty('id') ? (
      <OrderForm order={this.state.selectedOrder} cancelCallback={this.handleCancelForm} updateCallback={this.handleUpdate} />
    ) : (
      ""
    );
    return (
      <div className='padding'>
        <h2>Orders list</h2>
        <table className='ordersList'>
          <thead>
            <tr>
              <th>Order ID</th>
              <th>Customer</th>
              <th>Order total</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.state.ordersList.map(function(order, index){
              let uniqueKey = "order-" + order.id + "-" + index;
              return (
                <tr key={uniqueKey}>
                  <td><span>{order.id}</span></td>
                  <td><span>{_this.state.customersList[order['customer-id']].name}</span></td>
                  <td><span>$ {order.total}</span></td>
                  <td><button onClick={() => _this.handleClick(order)}>Edit</button></td>
                </tr>
              );
            })}
          </tbody>
        </table>
        {orderForm}
      </div>
    );
  }
}

export default OrderList;
