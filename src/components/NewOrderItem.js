import React, { Component } from 'react';
import products from '../api/products';

class NewOrderItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      'description': '',
      'product-id': '',
      'unit-price': '',
      'quantity': '',
      'total': '',
      'showForm': false
    };
    // for click events from DOM we need to bind `this` variable to keep the same context.
    this.saveNewItem = this.saveNewItem.bind(this);
    this.handleProductChange = this.handleProductChange.bind(this);
    this.handleQuantityChange = this.handleQuantityChange.bind(this);
    this.toggleForm = this.toggleForm.bind(this);
  }
  /**
   * Update component state when the selected product changes
   * @param {*} event 
   */
  handleProductChange(event) {
    let selectedProduct = {};
    products.forEach(function(item){
      if( item.id === event.target.value ) {
        selectedProduct = item;
      }
    });
    if( selectedProduct.hasOwnProperty('id') ) {
      this.setState({
        'product-id': selectedProduct.id,
        'description': selectedProduct.description,
        'unit-price': selectedProduct.price,
        'quantity': 1,
        'total': selectedProduct.price
      });
    }
  }
  /**
   * Update component state when the quantity changes and calculate the order item total.
   * @param {*} event 
   */
  handleQuantityChange(event) {
    let quantity = event.target.value;
    let totalPrice = quantity * this.state['unit-price'];
    totalPrice = parseFloat(Math.round(totalPrice * 100) / 100).toFixed(2);
    this.setState({quantity: quantity});
    this.setState({total: totalPrice});
  }
  /**
   * Toggle form visibility
   * @param {*} event 
   */
  toggleForm(event){
    event.preventDefault();
    this.setState({showForm: !this.state.showForm});
  }
  /**
   * Will save the new order item and call the parent's `onSaveItem` callback function;
   * Also, it will reset this component's state
   * @param {*} event 
   */
  saveNewItem(event) {
    event.preventDefault();
    let newItem = {
      'product-id': this.state['product-id'],
      'unit-price': this.state['unit-price'],
      'quantity': this.state.quantity,
      'total': this.state.total,
    };
    this.props.onSaveItem(newItem);
    this.setState({
      'description': '',
      'product-id': '',
      'unit-price': '',
      'quantity': '',
      'total': '',
      'showForm': false
    });
  }
  render() {
    return (
      <tbody>
        {this.state.showForm && 
          <tr>
            <td>
              <span>{this.state['product-id']}</span>
            </td>
            <td>
              <select name='product-id' value={this.state['product-id']} onChange={this.handleProductChange}>
                <option value=''>- Select product -</option>
                {products.map(function(item, index){
                  let optionKey = 'newItem-' + item.id + "-" + index;
                  return (
                    <option key={optionKey} value={item.id}>{item.description}</option>
                  );
                })}
              </select>
            </td>
            <td><span>
              {this.state['unit-price'] > 0 &&
                '$ '
              }
              {this.state['unit-price']}
            </span></td>
            <td>
              {this.state.quantity != '' &&
                <input type='number' step='1' name='quantity' value={this.state.quantity} size='3' onChange={this.handleQuantityChange} />
              }
            </td>
            <td><span>
              {this.state.total > 0 &&
                '$ '
              }
              {this.state.total}
            </span></td>
            <td><button onClick={this.saveNewItem}>Save item</button> <button onClick={this.toggleForm}>Remove item</button></td>
          </tr>
        }
        {!this.state.showForm && 
          <tr>
            <td colSpan='5'></td>
            <td><button onClick={this.toggleForm}>Add item</button></td>
          </tr>
        }
      </tbody>
    );
  }
}

export default NewOrderItem;
