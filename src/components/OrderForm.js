import React, { Component } from 'react';
import products from '../api/products';
import OrderItem from './OrderItem';
import NewOrderItem from './NewOrderItem';

class OrderForm extends Component {
  constructor(props) {
    super(props);
    this.state = props.order;
    // for click events from DOM we need to bind `this` variable to keep the same context.
    this.handleQuantityChange = this.handleQuantityChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNewItem = this.handleNewItem.bind(this);
  }
  /**
   * Update component state when new props are received
   * @param {*} newProps 
   */
  componentWillReceiveProps(newProps) {
    this.setState(newProps.order);
  }
  /**
   * Save order and call the parent `updateCallback` function
   * @param {*} event 
   */
  handleSubmit(event) {
    event.preventDefault();
    this.props.updateCallback(this.state);
  }
  /**
   * Update component state when quantity is changed
   * and recalculate order totals
   * @param {*} item 
   */
  handleQuantityChange(item) {
    let udpatedItems = [];
    let orderTotal = 0;
    for (var i = 0; i < this.state.items.length; i++) {
      if( this.state.items[i]['product-id'] === item['product-id'] ) {
        if( item.quantity > 0 ) {
          udpatedItems.push(item);
          orderTotal += parseFloat(item.total);
        }
      } else {
        udpatedItems.push(this.state.items[i]);
        orderTotal += parseFloat(this.state.items[i].total);
      }
    }
    orderTotal = parseFloat(Math.round(orderTotal * 100) / 100).toFixed(2);
    this.setState({
      items: udpatedItems,
      total: orderTotal
    });
  }
  /**
   * Update order items when a new item is added
   * This function will be called from `NewOrderItem` component when the user adds a new item.
   * If the added product already exist it's quantity will be updated.
   * Also, the order totals are recalculated.
   * @param {*} item 
   */
  handleNewItem(item) {
    let udpatedItems = [];
    let orderTotal = 0;
    let updatedItem = false;
    for (var i = 0; i < this.state.items.length; i++) {
      udpatedItems.push(this.state.items[i]);
      orderTotal += parseFloat(this.state.items[i].total);
      if( this.state.items[i]['product-id'] === item['product-id'] && item.quantity > 0 ) {
        orderTotal += parseFloat(item.total);
        udpatedItems[i].quantity = parseInt(this.state.items[i].quantity) + parseInt(item.quantity);
        udpatedItems[i].total = parseFloat(udpatedItems[i].total) + parseFloat(item.total);
        updatedItem = true;
      }
    }
    if(!updatedItem && item.quantity > 0) {
      udpatedItems.push(item);
      orderTotal += parseFloat(item.total);
    }
    orderTotal = parseFloat(Math.round(orderTotal * 100) / 100).toFixed(2);
    this.setState({
      items: udpatedItems,
      total: orderTotal
    });
  }

  render() {
    let _this = this;
    return (
      <div>
        <h2>Edit order #{this.state.id}</h2>
        <form onSubmit={this.handleSubmit}>
          <table className='full'>
            <thead>
              <tr>
                <th>Item ID</th><th>Description</th><th>Unit price</th><th>Quantity</th><th>Total</th><th></th>
              </tr>
            </thead>
            <tbody>
            {this.state.items.map(function(item, index){
              let uniqueKey = _this.state.id + "-" + item['product-id'] + "-" + index;
              return (
                <OrderItem key={uniqueKey} item={item} onQuantityChange={_this.handleQuantityChange} />
              );
            })}
            </tbody>
            <NewOrderItem onSaveItem={this.handleNewItem} />
            <tr className='bolder'>
              <td colSpan='4'>
                <span>Order total</span>
              </td>
              <td>
                <span>$ {this.state.total}</span>
              </td>
              <td></td>
            </tr>
          </table>
          <p>
            <input type="submit" value="Update" />
            <button onClick={this.props.cancelCallback}>Cancel</button>
          </p>
        </form>
      </div>
    );
  }
}

export default OrderForm;
